# Waracle Test

Basic Libraries Used  
- Retrofit  
- RXJava  
- Glide  
- Circleimageview   

Test Libraries  
- Mockito  
- JUnit  
- Roboelectric    

Architecture Used  
- MVP  
- MVVM Was considered but the initial setup time for MVVM is longer because of my better experience on MVP.  

Observations & Comments  

- Banana Image URL is an HTTP instead of an HTTPS. Not sure if this was on purpose. 3 Solutions available  
    - Chosen solution: usesCleartextTraffic set as true in manifest file. Can be not secure and allows all HTTP domains.  
    - usesCleartextTraffic in the network_security_config specifying the trusted domains. Added the class for proof but I didn't used it
    as I am not aware of any potential changes in the Image domains/urls.  
    - Proper solution: Never use HTTP  

- Retrofit wasn't really necessary as I could have used AsyncTask and normal Java requests.  
I used it as I believe it makes the code a lot cleaner and allows the project to be flexible.

- WARNING: The option setting 'android.jetifier.blacklist=shadows' is experimental and unsupported.  
 Affected Modules: app  
 You get this option as it was advised by Roboelectric library to add that setting because of issues with AndroidX  

- Testing  
The main presenter's functionality is been Tested for the following scenarios:  
 -Valid Data  
 -Empty Data  
 -Exception response (no internet, 404, 500, null response etc)    

- I only show technical errors in the dialogs when in debug mode. So if you run the app in release mode you'll see a client specific message like this
"Something went wrong, please try again"
