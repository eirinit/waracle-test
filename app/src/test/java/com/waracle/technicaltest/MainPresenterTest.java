package com.waracle.technicaltest;

import androidx.test.core.app.ApplicationProvider;

import com.waracle.technicaltest.model.APIService;
import com.waracle.technicaltest.model.response.Cake;
import com.waracle.technicaltest.presenter.MainPresenter;
import com.waracle.technicaltest.view.main.MainMvpView;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = "AndroidManifest.xml")
public class MainPresenterTest {

    MainPresenter presenter;
    MainMvpView mvpView;
    APIService apiService;

    @Before
    public void setUp() {
        App application = (App) RuntimeEnvironment.application;
        apiService = mock(APIService.class);
        // Mock the retrofit service so we don't call the API directly
        application.setApiService(apiService);
        // Change the default subscribe schedulers so all observables
        // will now run on the same thread
        application.setDefaultSubscribeScheduler(Schedulers.trampoline());
        mvpView = mock(MainMvpView.class);

        presenter = new MainPresenter(mvpView);
        when(mvpView.getContext()).thenReturn(application);
        presenter.attachView(mvpView);
    }

    @After
    public void tearDown() {
        presenter.detachView();
    }

    private List<Cake> newValidLiveData() {
        List<Cake> cakeList = new ArrayList<>();

        cakeList.add(new Cake("A Title", "A Description", "https://image.png"));
        cakeList.add(new Cake("A Title", "A Description", "https://image.png"));
        cakeList.add(new Cake("B Title", "B Description", "https://image.png"));
        cakeList.add(new Cake("C Title", "C Description", "https://image.png"));
        cakeList.add(new Cake("D Title", "D Description", "https://image.png"));
        cakeList.add(new Cake("E Title", "E Description", "https://image.png"));
        cakeList.add(new Cake("C Title", "C Description", "https://image.png"));
        return cakeList;
    }

    private List<Cake> newSortedLiveData() {
        List<Cake> cakeList = new ArrayList<>();

        cakeList.add(new Cake("A Title", "A Description", "https://image.png"));
        cakeList.add(new Cake("B Title", "B Description", "https://image.png"));
        cakeList.add(new Cake("C Title", "C Description", "https://image.png"));
        cakeList.add(new Cake("D Title", "D Description", "https://image.png"));
        cakeList.add(new Cake("E Title", "E Description", "https://image.png"));
        return cakeList;
    }

    @Test
    public void success() {

        List<Cake> cakeList = newValidLiveData();
        when(apiService.getCakes())
                .thenReturn(Observable.just(cakeList));

        presenter.getCakes();
        verify(mvpView).cakesLoaded(newSortedLiveData());
    }


    @Test
    public void emptyResponse() {

        List<Cake> cakeList = new ArrayList<>();
        when(apiService.getCakes())
                .thenReturn(Observable.just(cakeList));

        presenter.getCakes();
        verify(mvpView).showEmptyResponse();
    }

    @Test
    public void exception() {
        when(apiService.getCakes())
                .thenReturn(Observable.error(new Throwable()));

        presenter.getCakes();
        verify(mvpView).showMessage(ApplicationProvider.getApplicationContext().getString(R.string.something_went_wrong_please_try_again));
    }

    //TODO If our presenter threw specific exceptions for 401,403,404,500 etc we should have included a test per exception.

}
