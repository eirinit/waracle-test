package com.waracle.technicaltest.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.waracle.technicaltest.model.response.Cake;
import java.util.List;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;


public interface APIService {

    @GET("waracle_cake-android-client")
    Observable<List<Cake>> getCakes();

    class Factory {
        private static OkHttpClient getOkHttpClient() {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            return new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build();
        }

        public static APIService create() {
            Gson gson = new GsonBuilder().create();
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(APIConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
            Retrofit retrofit = builder.client(getOkHttpClient()).build();
            return retrofit.create(APIService.class);
        }
    }
}
