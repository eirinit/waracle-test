package com.waracle.technicaltest.model.response;

import com.google.gson.annotations.SerializedName;

public class Cake {


    /**
     * title : Lemon cheesecake
     * desc : A cheesecake made of lemon
     * image : https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg
     */

    @SerializedName("title")
    private String title;
    @SerializedName("desc")
    private String desc;
    @SerializedName("image")
    private String image;

    public Cake(String title, String desc, String image) {
        this.title = title;
        this.desc = desc;
        this.image = image;
    }
//TODO Would use lombok annotations instead of these getters and setters. https://projectlombok.org/features/GetterSetter

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    @Override
    public boolean equals(Object object) {
        Cake toCheck = (Cake) object;

        return toCheck.title.equals(title) && toCheck.desc.equals(desc) && toCheck.image.equals(image);
    }

    @Override
    public int hashCode() {
        return title.hashCode() + desc.hashCode() + image.hashCode();
    }
}
