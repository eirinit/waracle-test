package com.waracle.technicaltest.view.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.waracle.technicaltest.R;
import com.waracle.technicaltest.model.response.Cake;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CakeListAdapter extends RecyclerView.Adapter<CakeListAdapter.ViewHolder> {
    private List<Cake> cakeList;
    private Context context;
    private OnCakeItemClickListener onCakeItemClickListener;

    public CakeListAdapter(Context context, List<Cake> cakeList, OnCakeItemClickListener onCakeItemClickListener) {
        this.context = context;
        this.cakeList = cakeList;
        this.onCakeItemClickListener = onCakeItemClickListener;
    }

    public void updateData(List<Cake> cakeList) {
        this.cakeList = cakeList;
        notifyDataSetChanged();
    }

    @Override
    public CakeListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cake, parent, false);
        return new ViewHolder(v);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CircleImageView imgCake;
        public TextView txtCakeName;
        public RelativeLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            imgCake = itemView.findViewById(R.id.imgCake);
            txtCakeName = itemView.findViewById(R.id.txtCakeName);
            parentLayout = itemView.findViewById(R.id.parentLayout);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Cake cake = cakeList.get(position);
        holder.txtCakeName.setText(cake.getTitle());
        Glide.with(context)
                .load(cake.getImage())
                .into(holder.imgCake);

        holder.parentLayout.setOnClickListener(v -> onCakeItemClickListener.onCakeClicked(cake));

    }

    @Override
    public int getItemCount() {
        return cakeList.size();
    }

    public interface OnCakeItemClickListener {
        void onCakeClicked(Cake cake);
    }
}