package com.waracle.technicaltest.view.splash;

import android.os.Bundle;
import android.os.Handler;

import com.waracle.technicaltest.R;
import com.waracle.technicaltest.presenter.SplashPresenter;
import com.waracle.technicaltest.view.base.BaseActivity;

public class SplashActivity extends BaseActivity implements SplashMvpView {
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    SplashPresenter splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        splashPresenter = new SplashPresenter(this);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                startActivity(splashPresenter.getIntent(SplashActivity.this));
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public void performRequest() {
        //no requests needed in splash screen
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        splashPresenter.detachView();
    }
}
