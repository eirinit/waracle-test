package com.waracle.technicaltest.view.base;

import android.app.AlertDialog;
import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.waracle.technicaltest.R;
import com.waracle.technicaltest.view.MvpView;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements MvpView {

    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.progress_circular)
    ProgressBar progressBar;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        initContentView();
    }

    private void initContentView() {
        ButterKnife.bind(this);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showProgressIndicator() {
        if (progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressIndicator() {
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showEmptyResponse() {
        showAlertDialog(getString(R.string.no_data_found), getString(R.string.no_cakes_available), false);
    }

    @Override
    public void showMessage(String message) {
        hideProgressIndicator();
        showErrorDialog(message);
    }

    public void showErrorDialog(String message) {
        runOnUiThread(() -> showAlertDialog(getString(R.string.api_error_occurred), message, true)
                .create().show());
    }

    public AlertDialog.Builder showAlertDialog(String title, String message, Boolean allowRetry) {
        hideProgressIndicator();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message);
        builder.setCancelable(true);
        if (allowRetry) {
            builder.setPositiveButton(getString(R.string.retry), (dialog, which) -> performRequest());
        }
        return builder;
    }

}
