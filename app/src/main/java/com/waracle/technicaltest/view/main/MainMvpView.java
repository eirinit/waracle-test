package com.waracle.technicaltest.view.main;

import com.waracle.technicaltest.model.response.Cake;
import com.waracle.technicaltest.view.MvpView;

import java.util.List;

public interface MainMvpView extends MvpView {
    void cakesLoaded(List<Cake> cakes);

}
