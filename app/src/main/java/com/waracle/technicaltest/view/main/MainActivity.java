package com.waracle.technicaltest.view.main;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.waracle.technicaltest.R;
import com.waracle.technicaltest.model.response.Cake;
import com.waracle.technicaltest.presenter.MainPresenter;
import com.waracle.technicaltest.view.base.BaseActivity;
import com.waracle.technicaltest.view.main.adapter.CakeListAdapter;
import java.util.List;
import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements MainMvpView, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.recyclerViewCakes)
    RecyclerView recyclerViewCakes;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    CakeListAdapter cakeListAdapter;
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainPresenter = new MainPresenter(this);
        setupUI();
        performRequest();
    }

    private void setupUI() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewCakes.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerViewCakes.getContext(),
                layoutManager.getOrientation());
        recyclerViewCakes.addItemDecoration(dividerItemDecoration);
        recyclerViewCakes.setItemAnimator(new DefaultItemAnimator());

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorSecondary,
                R.color.colorBgSplashCenter);
    }

    @Override
    public void performRequest() {
        mainPresenter.getCakes();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detachView();
    }

    @Override
    public void showErrorDialog(String message) {
        super.showErrorDialog(message);
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void cakesLoaded(List<Cake> cakes) {
        if (cakeListAdapter == null) {
            cakeListAdapter = new CakeListAdapter(this, cakes, this::showCakeDetails);
            recyclerViewCakes.setAdapter(cakeListAdapter);
            recyclerViewCakes.scheduleLayoutAnimation();
            recyclerViewCakes.invalidate();

        } else {
            cakeListAdapter.updateData(cakes);
            recyclerViewCakes.scheduleLayoutAnimation();
            recyclerViewCakes.invalidate();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onRefresh() {
        performRequest();
    }


    private void showCakeDetails(Cake cake) {

        //TODO In a real project I would create a proper class for creating the dialog either using a DialogFragment or Custom AlertDialog
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_cake_details, null);
        CircleImageView imgCake = dialogView.findViewById(R.id.imgCake);
        TextView txtCakeName= dialogView.findViewById(R.id.txtCakeName);
        TextView txtCakeDesc= dialogView.findViewById(R.id.txtCakeDesc);
        ImageButton btnClose = dialogView.findViewById(R.id.btnClose);
        txtCakeDesc.setText(cake.getDesc());
        txtCakeName.setText(cake.getTitle());
        Glide.with(this)
                .load(cake.getImage())
                .into(imgCake);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        btnClose.setOnClickListener(v -> alertDialog.dismiss());
    }


}

