package com.waracle.technicaltest.view;

import android.content.Context;

public interface MvpView {
    //TODO These look too much now that we only have one activity. It's always a good practise to develop with future maintainability in mind though

    Context getContext();

    void showMessage(String message);

    void showProgressIndicator();

    void hideProgressIndicator();

    void showEmptyResponse();

    void performRequest();
}
