package com.waracle.technicaltest;

import android.app.Application;
import android.content.Context;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import com.waracle.technicaltest.model.APIService;

public class App extends Application {
    private Scheduler defaultSubscribeScheduler;
    private APIService apiService;


    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }

    public void setDefaultSubscribeScheduler(Scheduler defaultSubscribeScheduler) {
        this.defaultSubscribeScheduler = defaultSubscribeScheduler;
    }

    public void setApiService(APIService apiService) {
        this.apiService = apiService;
    }

    public APIService getAPIService() {
        if (apiService == null)
            apiService = APIService.Factory.create();
        return apiService;
    }

}
