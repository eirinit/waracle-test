package com.waracle.technicaltest.presenter;


import com.waracle.technicaltest.R;
import com.waracle.technicaltest.model.response.Cake;
import com.waracle.technicaltest.presenter.base.BasePresenter;
import com.waracle.technicaltest.view.main.MainMvpView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class MainPresenter extends BasePresenter<MainMvpView> {

    public MainPresenter(MainMvpView mvpView) {
        super(mvpView);
    }

    public void getCakes() {

        getMvpView().showProgressIndicator();
        getCompositeDisposable().add(getApiService().getCakes()
                .observeOn(getObservedOn())
                .flatMap(removeDuplicatesAndSort())
                .subscribeOn(getDefaultSubscribeScheduler())
                .filter(response -> isViewAttached())
                .subscribe(response -> {
                    if(response.isEmpty()){
                        getMvpView().showEmptyResponse();
                    }else{
                        getMvpView().cakesLoaded(response);

                    }
                    getMvpView().hideProgressIndicator();

                }, throwable -> {
                    getMvpView().hideProgressIndicator();
                    handleError(throwable, R.string.something_went_wrong_please_try_again);
                }));
    }

    private Function<List<Cake>,
            ObservableSource<List<Cake>>> removeDuplicatesAndSort() {
        return response -> {

            if (response != null) {
                //TODO Would use a helper class to include this functionality. Should not be in the presenter

                // Create as set. Set will contain only unique objects
                HashSet hashSet = new HashSet(response);

                response = new ArrayList<>();
                response.addAll(hashSet);

                //Sort list
                Collections.sort(response, new Comparator<Cake>() {
                    public int compare(Cake obj1, Cake obj2) {
                        return obj1.getTitle().compareToIgnoreCase(obj2.getTitle());
                    }
                });
                return Observable.just(response);
            }
            return Observable.error(new Throwable());
        };
    }

}
