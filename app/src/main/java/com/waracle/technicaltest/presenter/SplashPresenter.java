package com.waracle.technicaltest.presenter;

import android.content.Context;
import android.content.Intent;
import com.waracle.technicaltest.view.main.MainActivity;
import com.waracle.technicaltest.presenter.base.BasePresenter;
import com.waracle.technicaltest.view.splash.SplashMvpView;

public class SplashPresenter extends BasePresenter<SplashMvpView> {
    public SplashPresenter(SplashMvpView mvpView) {
        super(mvpView);
    }

    @Override
    public void attachView(SplashMvpView view) {
        super.attachView(view);
    }

    /* TODO This method looks unnecessary now but in the event
    we want to open a different activity based on user's
    login status in the future it's a good practise to include it initially.*/
    public Intent getIntent(Context context) {
        Intent i = new Intent(context,  MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return i;
    }
}
