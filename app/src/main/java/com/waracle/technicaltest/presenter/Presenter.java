package com.waracle.technicaltest.presenter;

public interface Presenter<V> {

    void attachView(V view);

    void detachView();
}
