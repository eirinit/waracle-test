package com.waracle.technicaltest.presenter.base;

import android.content.Context;

import com.waracle.technicaltest.App;
import com.waracle.technicaltest.BuildConfig;
import com.waracle.technicaltest.model.APIService;
import com.waracle.technicaltest.presenter.Presenter;
import com.waracle.technicaltest.view.MvpView;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<MvpViewType extends MvpView> implements Presenter<MvpViewType> {

    private MvpViewType mvpView;
    private CompositeDisposable compositeDisposable;

    public BasePresenter(MvpViewType mvpView) {
        compositeDisposable = new CompositeDisposable();

        this.mvpView = mvpView;
        attachView(mvpView);
    }

    @Override
    public void attachView(MvpViewType view) {
        this.mvpView = view;
    }

    @Override
    public void detachView() {
        if (compositeDisposable != null)
            compositeDisposable.dispose();
        this.mvpView = null;
    }

    protected CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    protected Scheduler getObservedOn() {
        return AndroidSchedulers.mainThread();
    }

    public boolean isViewAttached() {
        return mvpView != null;
    }

    public MvpViewType getMvpView() {
        return mvpView;
    }

    public Context getContext() {
        if (mvpView != null)
            return mvpView.getContext();
        return null;
    }

    public Scheduler getDefaultSubscribeScheduler() {
        if (getContext().getApplicationContext() instanceof App) {
            App application = (App) getContext().getApplicationContext();
            return application.defaultSubscribeScheduler();
        }
        return null;
    }

    public APIService getApiService() {
        App application = App.get(getContext());
        return application.getAPIService();
    }

    public String getString(int stringResId) {
        if (getContext() != null)
            return getContext().getString(stringResId);
        return null;
    }

    public void handleError(Throwable error, Integer httpDefaultErrorResId) {
        if (BuildConfig.DEBUG && error.getLocalizedMessage() != null) {
            getMvpView().showMessage(error.getLocalizedMessage());

        } else {
            getMvpView().showMessage(getString(httpDefaultErrorResId));

        }
        //TODO in an actual project we would try to figure out the type of error and retry/show appropriate error to the user.

    }


}
